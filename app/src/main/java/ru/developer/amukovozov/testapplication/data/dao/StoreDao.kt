package ru.developer.amukovozov.testapplication.data.dao

import android.arch.persistence.room.*
import ru.developer.amukovozov.testapplication.data.entity.Manager
import ru.developer.amukovozov.testapplication.data.entity.Store

@Dao
interface StoreDao {

    @Query("SELECT * from Store")
    fun getAll(): List<Store>

    @Query("SELECT * from Store where storeId == :id")
    fun getById(id: Long) : Store

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(store: Store) : Long

    @Delete
    fun delete(store: Store)

    @Query("DELETE from Store")
    fun deleteAll()
}