package ru.developer.amukovozov.testapplication

import android.support.v4.app.Fragment
import ru.developer.amukovozov.testapplication.ui.MainFlowFragment
import ru.developer.amukovozov.testapplication.ui.charts.ChartsFragment
import ru.developer.amukovozov.testapplication.ui.store.StoreFlowFragment
import ru.developer.amukovozov.testapplication.ui.store.StoreInformationFragment
import ru.developer.amukovozov.testapplication.ui.store.create.StoreCreateFragment
import ru.developer.amukovozov.testapplication.ui.storelist.StoreListFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen

object Screens {

    object MainFlow : SupportAppScreen() {
        override fun getFragment(): Fragment = MainFlowFragment()
    }
    object StoresFlow : SupportAppScreen() {
        override fun getFragment(): Fragment = StoreListFragment()
    }
    object ChartsFlow : SupportAppScreen() {
        override fun getFragment(): Fragment = ChartsFragment()
    }

//    data class StoreFlow(
//        val storeId: Long
//    ): SupportAppScreen() {
//        override fun getFragment(): Fragment = StoreFlowFragment.create(storeId)
//    }

    data class StoreInfoFlow(
    val storeId: Long
    ): SupportAppScreen() {
        override fun getFragment(): Fragment = StoreInformationFragment.create(storeId)
    }

    data class StoreCreateFlow(
        val storeId: Long = 0
    ): SupportAppScreen() {
        override fun getFragment(): Fragment = StoreCreateFragment.create(storeId)
    }
}