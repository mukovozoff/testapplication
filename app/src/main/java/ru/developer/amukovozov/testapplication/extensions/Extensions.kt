package ru.developer.amukovozov.testapplication.extensions

import android.content.Context
import android.support.annotation.LayoutRes
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.AppCompatEditText
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.arellomobile.mvp.MvpAppCompatActivity
import ru.developer.amukovozov.testapplication.App
import ru.developer.amukovozov.testapplication.di.component.AppComponent
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.android.support.SupportAppScreen
import ru.terrakok.cicerone.commands.BackTo
import ru.terrakok.cicerone.commands.Replace

fun AppCompatActivity.getAppComponent(): AppComponent =
    (applicationContext as App).appComponent

fun Fragment.getAppComponent(): AppComponent =
    (activity?.applicationContext as App).appComponent

fun Navigator.setLaunchScreen(screen: SupportAppScreen) {
    applyCommands(
        arrayOf(
            BackTo(null),
            Replace(screen)
        )
    )
}

fun View.visible(visible: Boolean) {
    this.visibility = if (visible) View.VISIBLE else View.GONE
}

fun Context.color(colorRes: Int) = ContextCompat.getColor(this, colorRes)

fun AppCompatEditText.textInString() =
    text.toString()

fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
}