package ru.developer.amukovozov.testapplication.domain

import ru.developer.amukovozov.testapplication.di.PrimitiveWrapper
import ru.developer.amukovozov.testapplication.domain.server.StubApi
import ru.developer.amukovozov.testapplication.domain.system.DefaultPageSize
import ru.developer.amukovozov.testapplication.domain.system.SchedulersProvider

class StoreRepositoryFromServer(private val api: StubApi,
                                private val schedulers: SchedulersProvider,
                                @DefaultPageSize private val defaultPageSizeWrapper: PrimitiveWrapper<Int>
) {
    private val defaultPageSize = defaultPageSizeWrapper.value

//    fun getStoresList(page: Int, pageSize: Int = defaultPageSize) =

}