package ru.developer.amukovozov.testapplication.domain.server

import io.reactivex.Single
import ru.developer.amukovozov.testapplication.data.entity.Store

class ApiWithCache(private val serverApi: StubApi,
                   private val storeCache: StoreCache
) : StubApi by serverApi {

    override fun getStores(page: Int): Single<List<Store>> =
            serverApi.getStores(page)
                .doOnSuccess { storeCache.put(it) }
}