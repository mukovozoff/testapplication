package ru.developer.amukovozov.testapplication.ui.global

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.MvpAppCompatFragment
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import ru.developer.amukovozov.testapplication.data.entity.Store
import timber.log.Timber

private const val PROGRESS_TAG = "bf_progress"

abstract class BaseFragment : MvpAppCompatFragment() {

    abstract val layoutRes: Int

    private var instanceStateSaved: Boolean = false

    private val viewHandler = Handler()

    companion object {
        val databaseSubject = PublishSubject.create<Store>()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(layoutRes, container, false)

    override fun onResume() {
        super.onResume()
//        databaseSubject.subscribe {
//            Timber.log(0, "hello")
//        }
        instanceStateSaved = false
    }

//    Wrong work with async views
    protected fun postViewAction(action: () -> Unit) {
        viewHandler.post(action)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        viewHandler.removeCallbacksAndMessages(null)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        instanceStateSaved = true
    }

    protected fun showProgressDialog(isLoading: Boolean) {
        if (isAdded || instanceStateSaved) return

        val fragment = childFragmentManager.findFragmentByTag(PROGRESS_TAG)
        if (fragment != null && !isLoading) {
            (fragment as ProgressDialog).dismissAllowingStateLoss()
            childFragmentManager.executePendingTransactions()
        } else if (fragment == null && isLoading) {
            ProgressDialog().show(childFragmentManager, PROGRESS_TAG)
        }
    }

    open fun onBackPressed() {}
}