package ru.developer.amukovozov.testapplication.domain.server

import ru.developer.amukovozov.testapplication.data.entity.Store
import ru.developer.amukovozov.testapplication.di.PrimitiveWrapper
import ru.developer.amukovozov.testapplication.domain.system.CacheLifetime
import timber.log.Timber
import java.util.concurrent.ConcurrentHashMap

class StoreCache(@CacheLifetime lifetimeWrapper: PrimitiveWrapper<Long>
) {
    private val lifetime = lifetimeWrapper.value

    private data class StoreCacheItem(val time: Long, val data: Store)

    private val cache = ConcurrentHashMap<Long, StoreCacheItem>()

    fun clear() {
        Timber.d("Clear cache")
        cache.clear()
    }

    fun put(data: List<Store>) {
        Timber.d("Put stores")
        cache.putAll(data
            .asSequence()
            .map { StoreCacheItem(System.currentTimeMillis(), it)}
            .associateBy { it.data.storeId })
    }

    fun get(id: Long): Store? {
        val item = cache[id]
        return if (item == null || System.currentTimeMillis() - item.time > lifetime) {
            Timber.d("Get NULL project($id)")
            null
        } else {
            Timber.d("Get CACHED project($id)")
            item.data
        }
    }
}