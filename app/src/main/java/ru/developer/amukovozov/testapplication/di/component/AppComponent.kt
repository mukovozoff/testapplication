package ru.developer.amukovozov.testapplication.di.component

import android.content.Context
import dagger.Component
import ru.developer.amukovozov.testapplication.ui.AppActivity
import ru.developer.amukovozov.testapplication.data.AppDatabase
import ru.developer.amukovozov.testapplication.di.PrimitiveWrapper
import ru.developer.amukovozov.testapplication.di.module.AppModule
import ru.developer.amukovozov.testapplication.di.module.DatabaseModule
import ru.developer.amukovozov.testapplication.di.module.NavigationModule
import ru.developer.amukovozov.testapplication.di.module.StorageModule
import ru.developer.amukovozov.testapplication.domain.interactor.StockInteractor
import ru.developer.amukovozov.testapplication.domain.interactor.StoreInteractor
import ru.developer.amukovozov.testapplication.domain.repository.StoreRepository
import ru.developer.amukovozov.testapplication.domain.storage.RawAppData
import ru.developer.amukovozov.testapplication.domain.system.ResourceManager
import ru.developer.amukovozov.testapplication.domain.system.SchedulersProvider
import ru.developer.amukovozov.testapplication.domain.system.ServerPath
import ru.developer.amukovozov.testapplication.ui.charts.ChartsFragment
import ru.developer.amukovozov.testapplication.ui.store.StoreFlowFragment
import ru.developer.amukovozov.testapplication.ui.store.StoreInformationFragment
import ru.developer.amukovozov.testapplication.ui.store.create.StoreCreateFragment
import ru.developer.amukovozov.testapplication.ui.storelist.StoreListFragment
import ru.terrakok.cicerone.Router
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, DatabaseModule::class, NavigationModule::class, StorageModule::class])
interface AppComponent {

    fun inject(view: AppActivity) : AppActivity

    fun inject(view: StoreListFragment) : StoreListFragment

    fun inject(view: StoreFlowFragment) : StoreFlowFragment

    fun inject(view: StoreInformationFragment) : StoreInformationFragment

    fun inject(view: StoreCreateFragment) : StoreCreateFragment

    fun inject(view: ChartsFragment) : ChartsFragment

    fun getContext() : Context

    fun getDatabase() : AppDatabase

    fun getRouter() : Router

    fun getDefaultPageSize() : PrimitiveWrapper<Int>

    fun getCacheLifetime() : PrimitiveWrapper<Long>

    fun getAppSchedulers() : SchedulersProvider

    @ServerPath
    fun getServerPath() : String

    fun getResourceManager() : ResourceManager

    fun getRawAppData() : RawAppData

    fun getStoreRepository() : StoreRepository

    fun getStoreInteractor() : StoreInteractor

    fun getStockInteractor() : StockInteractor
}