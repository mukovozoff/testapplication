package ru.developer.amukovozov.testapplication.ui.store

import android.os.Bundle
import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import ru.developer.amukovozov.testapplication.R
import ru.developer.amukovozov.testapplication.Screens
import ru.developer.amukovozov.testapplication.extensions.argument
import ru.developer.amukovozov.testapplication.extensions.getAppComponent
import ru.developer.amukovozov.testapplication.extensions.setLaunchScreen
import ru.developer.amukovozov.testapplication.presentation.store.StoreFlowPresenter
import ru.developer.amukovozov.testapplication.ui.global.BaseFragment
import ru.developer.amukovozov.testapplication.ui.global.FlowFragment
import ru.terrakok.cicerone.Router
import javax.inject.Inject

class StoreFlowFragment : FlowFragment(), MvpView {

//    override val layoutRes: Int = R.layout.fragment_store_flow
    private val storeId: Long by argument(ARG_STORE_ID, 0)

    @InjectPresenter
    lateinit var presenter: StoreFlowPresenter

    @Inject
    lateinit var router: Router

    @ProvidePresenter
    fun providePresenter(): StoreFlowPresenter = StoreFlowPresenter(router)


    override fun onCreate(savedInstanceState: Bundle?) {
        getAppComponent().inject(this)
        super.onCreate(savedInstanceState)

//        if (childFragmentManager.fragments.isEmpty()) {
//            navigator.setLaunchScreen(Screens.StoreInfoFlow)
//        }
    }

    override fun onExit() {
        presenter.onExit()
    }

    companion object {
        private const val ARG_STORE_ID = "arg_store_id"

        fun create(storeId: Long) = StoreFlowFragment().apply {
            arguments = Bundle().apply {
                putLong(ARG_STORE_ID, storeId)
            }
        }
    }
}