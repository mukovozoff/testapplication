package ru.developer.amukovozov.testapplication.domain.storage

import android.content.res.AssetManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.reactivex.Observable
import io.reactivex.Single
import ru.developer.amukovozov.testapplication.data.entity.Store
import ru.developer.amukovozov.testapplication.domain.server.StubApi
import java.io.InputStreamReader
import javax.inject.Inject

class RawAppData @Inject constructor(
    private val assets: AssetManager,
    private val gson: Gson
) {

    fun getStores(page: Int): Observable<List<Store>> = fromAsset("store.json")

    private inline fun <reified T> fromAsset(pathToAsset: String) = Single.defer {
        assets.open(pathToAsset).use { stream ->
            Single.just<T>(gson.fromJson(InputStreamReader(stream), object : TypeToken<T>() {}.type))
        }
    }.toObservable()
}