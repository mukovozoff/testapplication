package ru.developer.amukovozov.testapplication.presentation.store

import android.widget.Toast
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpView
import ru.developer.amukovozov.testapplication.di.PrimitiveWrapper
import ru.developer.amukovozov.testapplication.domain.interactor.StoreInteractor
import ru.developer.amukovozov.testapplication.presentation.global.BasePresenter
import ru.developer.amukovozov.testapplication.ui.global.BaseFragment
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class StoreInformationPresenter @Inject constructor(
    storeIdWrapper: PrimitiveWrapper<Long>,
    private val storeInteractor: StoreInteractor
) : BasePresenter<StoreInformationView>() {

    private val storeId = storeIdWrapper.value

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        loadStores()
    }

    override fun onCreate() {
        databaseSubject.subscribe {
            loadStores()
        }.connect()
    }

    fun loadStores() {
        storeInteractor
            .getStore(storeId)
            .doOnSubscribe { viewState.showProgress(true) }
            .doAfterTerminate { viewState.showProgress(false) }
            .subscribe(
                { store -> viewState.showStore(store) },
                { error -> viewState.showMessage(error.message!!) }
            )
            .connect()
    }
}