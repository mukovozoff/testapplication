package ru.developer.amukovozov.testapplication.presentation.storelist

import com.arellomobile.mvp.InjectViewState
import ru.developer.amukovozov.testapplication.Screens
import ru.developer.amukovozov.testapplication.data.entity.Store
import ru.developer.amukovozov.testapplication.domain.interactor.StoreInteractor
import ru.developer.amukovozov.testapplication.presentation.global.BasePresenter
import ru.developer.amukovozov.testapplication.presentation.global.Paginator
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class StoreListPresenter @Inject constructor(
    private val router: Router,
    private val interactor: StoreInteractor
) : BasePresenter<StoreListView>() {

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        refreshStores()
    }

    override fun onCreate() {
        databaseSubject.subscribe {
            refreshStores()
        }.connect()
    }

    private val paginator = Paginator(
    {getStoresSingle(it)},
        object : Paginator.ViewController<Store> {
            override fun showEmptyProgress(show: Boolean) {
                viewState.showEmptyProgress(show)
            }

            override fun showEmptyView(show: Boolean) {
                viewState.showEmptyView(show)
            }

            override fun showEmptyError(show: Boolean, error: Throwable?) {
//              TODO realize error handler with custom string for every error code and show message for user
                if (error != null) {
                    viewState.showEmptyError(show, error.message)
                } else {
                    viewState.showEmptyError(show, null)
                }
            }

            override fun showData(show: Boolean, data: List<Store>) {
                viewState.showStores(show, data)
            }

            override fun showErrorMessage(error: Throwable) {
//                TODO error handler for this errors need to be realized too
                viewState.showErrorMessage(error)
            }

            override fun showRefreshProgress(show: Boolean) {
                viewState.showRefreshProgress(show)
            }

            override fun showPageProgress(show: Boolean) {
                viewState.showPageProgress(show)
            }
        })


    private fun getStoresSingle(page: Int) =
            interactor.getStores(page)

    override fun onDestroy() {
        super.onDestroy()
        paginator.release()
    }

    fun refreshStores() = paginator.refresh()
    fun loadNextStoresPage() = paginator.loadNewPage()

    fun onStoreClicked(id: Long) {
        router.navigateTo(Screens.StoreInfoFlow(id))
    }

    fun onCreateStoreClicked() {
        router.navigateTo(Screens.StoreCreateFlow())
    }
    
    fun onBackPressed() = router.exit()

}