package ru.developer.amukovozov.testapplication.domain.repository

import io.reactivex.Observable
import io.reactivex.Single
import ru.developer.amukovozov.testapplication.data.AppDatabase
import ru.developer.amukovozov.testapplication.data.entity.Store
import ru.developer.amukovozov.testapplication.domain.storage.RawAppData
import ru.developer.amukovozov.testapplication.domain.system.SchedulersProvider
import javax.inject.Inject

class StoreRepository @Inject constructor(
    private val rawAppData: RawAppData,
    private val schedulers: SchedulersProvider,
    private val database: AppDatabase
) {

    fun getStoresFromDB(page: Int) = Observable.fromCallable {
        database.storeDao().getAll()
    }.subscribeOn(schedulers.io())

    fun getStoresFromJSON(page: Int) = rawAppData
        .getStores(page)
        .onErrorReturn { emptyList() }
        .subscribeOn(schedulers.io())
        .observeOn(schedulers.ui())
        .doOnNext{
            saveStoresToDB(it)
        }

    fun getStores(page: Int) = Observable.concat(getStoresFromDB(page), getStoresFromJSON(page))
        .subscribeOn(schedulers.io())
        .observeOn(schedulers.ui())
        .filter{it.isNotEmpty()}
        .first(emptyList())



    private fun saveStoresToDB(storesFromJSON: List<Store>) {
        Observable.fromCallable {
            val storeDao = database.storeDao()
            storesFromJSON.forEach { storeDao.insert(it) }
        }
            .subscribeOn(schedulers.io())
            .subscribe()
    }

    fun getStore(storeId: Long) = Observable.fromCallable {
            database.storeDao().getById(storeId)
        }
        .subscribeOn(schedulers.io())
        .observeOn(schedulers.ui())

    fun saveStore(store: Store) = Observable.fromCallable {
        database.storeDao().insert(store)
    }
        .subscribeOn(schedulers.io())
        .observeOn(schedulers.ui())
}