package ru.developer.amukovozov.testapplication.ui.global.list

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.hannesdorfmann.adapterdelegates3.AdapterDelegate
import kotlinx.android.synthetic.main.item_store.view.*
import ru.developer.amukovozov.testapplication.R
import ru.developer.amukovozov.testapplication.data.entity.Store
import ru.developer.amukovozov.testapplication.extensions.inflate

class StoresAdapterDelegate(private val clickListener: (Store) -> Unit) : AdapterDelegate<MutableList<Any>>() {

    override fun isForViewType(items: MutableList<Any>, position: Int): Boolean =
        items[position] is Store

    override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        val root = parent.inflate(R.layout.item_store)

        return ViewHolder(root)
    }

    override fun onBindViewHolder(
        items: MutableList<Any>,
        position: Int,
        holder: RecyclerView.ViewHolder,
        payloads: MutableList<Any>
    ) =
        (holder as ViewHolder).bind(items[position] as Store)

    private inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private lateinit var store: Store

        init {
            view.setOnClickListener { clickListener(store) }
        }

        fun bind(store: Store) {
            this.store = store
            with(itemView) {
                titleTextView.text = store.name
                storeEmail.text = store.email
                storeManager.text = store.manager.getFullName()
            }
        }
    }
}