package ru.developer.amukovozov.testapplication.data.entity

import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class Store(@PrimaryKey(autoGenerate = true) var storeId: Long = 0,
                 @SerializedName("Store") val name: String,
                 @SerializedName("Country Code") val countryCode: String,
                 @SerializedName("Store Email") val email: String,
                 @SerializedName("Manager") @Embedded val manager: Manager,
                 @SerializedName("Category") val category: Int?,
                 @SerializedName("On-Floor-Availability") val onFloorAvailability: Double?,
                 @SerializedName("Stock") @Embedded val stock: Stock)