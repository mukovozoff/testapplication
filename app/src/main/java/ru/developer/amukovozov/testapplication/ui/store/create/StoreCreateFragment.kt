package ru.developer.amukovozov.testapplication.ui.store.create

import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.activity_main.view.*
import kotlinx.android.synthetic.main.fragment_store_create.*
import ru.developer.amukovozov.testapplication.R
import ru.developer.amukovozov.testapplication.data.entity.Manager
import ru.developer.amukovozov.testapplication.data.entity.Stock
import ru.developer.amukovozov.testapplication.data.entity.Store
import ru.developer.amukovozov.testapplication.di.PrimitiveWrapper
import ru.developer.amukovozov.testapplication.domain.interactor.StoreInteractor
import ru.developer.amukovozov.testapplication.extensions.argument
import ru.developer.amukovozov.testapplication.extensions.getAppComponent
import ru.developer.amukovozov.testapplication.extensions.textInString
import ru.developer.amukovozov.testapplication.extensions.visible
import ru.developer.amukovozov.testapplication.presentation.store.StoreInformationPresenter
import ru.developer.amukovozov.testapplication.presentation.store.create.StoreCreatePresenter
import ru.developer.amukovozov.testapplication.presentation.store.create.StoreCreateView
import ru.developer.amukovozov.testapplication.ui.global.BaseFragment
import ru.developer.amukovozov.testapplication.ui.store.StoreInformationFragment
import ru.terrakok.cicerone.Router
import timber.log.Timber
import javax.inject.Inject

class StoreCreateFragment : BaseFragment(), StoreCreateView {

    override val layoutRes: Int = R.layout.fragment_store_create

    private val storeId: Long by argument(StoreCreateFragment.ARG_STORE_ID, 0)

    @InjectPresenter
    lateinit var presenter: StoreCreatePresenter

    @Inject
    lateinit var router: Router

    @Inject
    lateinit var storeInteractor: StoreInteractor

    private lateinit var store: Store

    @ProvidePresenter
    fun providePresenter(): StoreCreatePresenter = StoreCreatePresenter(
        PrimitiveWrapper(storeId),
        storeInteractor,
        router
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        getAppComponent().inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        createStore.setOnClickListener {
            buildStore()
//            вью не должна  знать что делать, просто передает презентеру событие
            presenter.validateStoreFields(store)
                .subscribe(
                    { createStore(it) },
                    { error -> showMessage(error.message!!)}
                )
        }
    }

    override fun createStore(store: Store) {
        presenter.createStore(store)
    }

    override fun showProgress(show: Boolean) {
        showProgressDialog(show)
        storeCreateLayout.visible(!show)
    }

    override fun showStore(store: Store) {
        this.store = store
        storeNameEdit.setText(store.name)
        countryCodeEdit.setText(store.countryCode)
        storeEmailEdit.setText(store.email)
        storeManagerFirstNameEdit.setText(store.manager.firstName)
        storeManagerLastNameEdit.setText(store.manager.lastName)
        storeManagerEmailEdit.setText(store.manager.managerEmail)
        storeCategoryEdit.setText(store.category.toString())
        storeStockBackStoreEdit.setText(store.stock.backStore.toString())
        storeStockFrontStoreEdit.setText(store.stock.frontStore.toString())
        storeStockShoppingWindowEdit.setText(store.stock.shoppingWindow.toString())
        storeStockAccuracyEdit.setText(store.stock.accuracy.toString())
        storeOnFloorAvailabilityEdit.setText(store.onFloorAvailability.toString())
        storeStockMeanAgeEdit.setText(store.stock.meanAgeDays.toString())
    }

    private fun buildStore() {
        if (storeId == 0L) {
            store = Store(
                name = storeNameEdit.textInString(),
                countryCode = countryCodeEdit.textInString(),
                email = storeEmailEdit.textInString(),
                manager = Manager(
                    firstName = storeManagerFirstNameEdit.textInString(),
                    lastName = storeManagerLastNameEdit.textInString(),
                    managerEmail = storeManagerEmailEdit.textInString()
                ),
                category = storeCategoryEdit.textInString().toIntOrNull(),
                onFloorAvailability = storeOnFloorAvailabilityEdit.textInString().toDoubleOrNull(),
                stock = Stock(
                    backStore = storeStockBackStoreEdit.textInString().toIntOrNull(),
                    frontStore = storeStockFrontStoreEdit.textInString().toIntOrNull(),
                    shoppingWindow = storeStockShoppingWindowEdit.textInString().toIntOrNull(),
                    accuracy = storeStockAccuracyEdit.textInString().toDoubleOrNull(),
                    meanAgeDays = storeStockMeanAgeEdit.textInString().toIntOrNull()
                )
            )
        } else {
            store = Store(
                storeId = store.storeId,
                name = storeNameEdit.textInString(),
                countryCode = countryCodeEdit.textInString(),
                email = storeEmailEdit.textInString(),
                manager = Manager(
                    firstName = storeManagerFirstNameEdit.textInString(),
                    lastName = storeManagerLastNameEdit.textInString(),
                    managerEmail = storeManagerEmailEdit.textInString()
                ),
                category = storeCategoryEdit.textInString().toIntOrNull(),
                onFloorAvailability = storeOnFloorAvailabilityEdit.textInString().toDoubleOrNull(),
                stock = Stock(
                    backStore = storeStockBackStoreEdit.textInString().toIntOrNull(),
                    frontStore = storeStockFrontStoreEdit.textInString().toIntOrNull(),
                    shoppingWindow = storeStockShoppingWindowEdit.textInString().toIntOrNull(),
                    accuracy = storeStockAccuracyEdit.textInString().toDoubleOrNull(),
                    meanAgeDays = storeStockMeanAgeEdit.textInString().toIntOrNull()
                )
            )
        }
    }

    override fun showMessage(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    companion object {
        private const val ARG_STORE_ID = "arg_store_id"

        fun create(storeId: Long) = StoreCreateFragment().apply {
            arguments = Bundle().apply {
                putLong(ARG_STORE_ID, storeId)
            }
        }
    }
}