package ru.developer.amukovozov.testapplication.presentation.store

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import ru.developer.amukovozov.testapplication.data.entity.Store

interface StoreInformationView : MvpView {

    fun showStore(store: Store)
    fun showProgress(show: Boolean)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showMessage(message: String)
}