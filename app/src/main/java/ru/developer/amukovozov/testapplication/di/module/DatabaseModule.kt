package ru.developer.amukovozov.testapplication.di.module

import android.arch.persistence.room.Database
import dagger.Module
import dagger.Provides
import ru.developer.amukovozov.testapplication.data.AppDatabase
import javax.inject.Singleton

@Module
class DatabaseModule(private val appDatabase: AppDatabase) {

    @Provides
    @Singleton
    internal fun provideDatabase() = appDatabase
}