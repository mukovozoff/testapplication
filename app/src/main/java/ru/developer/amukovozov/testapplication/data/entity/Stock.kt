package ru.developer.amukovozov.testapplication.data.entity

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class Stock(@PrimaryKey(autoGenerate = true) var stockId: Long = 0,
                 @SerializedName("Backstore") val backStore: Int?,
                 @SerializedName("Frontstore") val frontStore: Int?,
                 @SerializedName("Shopping Window") val shoppingWindow: Int?,
                 @SerializedName("Accuracy") val accuracy: Double?,
                 @SerializedName("Mean Age") val meanAgeDays: Int?)