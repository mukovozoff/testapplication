package ru.developer.amukovozov.testapplication.ui.charts

import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.formatter.IValueFormatter
import com.github.mikephil.charting.utils.ViewPortHandler
import ru.developer.amukovozov.testapplication.data.entity.Store

class StoreNameValueFormatter(val storeNames: List<String>) : IValueFormatter {
    override fun getFormattedValue(
        value: Float,
        entry: Entry?,
        dataSetIndex: Int,
        viewPortHandler: ViewPortHandler?
    ): String {
        return storeNames[entry!!.x.toInt()]
    }
}