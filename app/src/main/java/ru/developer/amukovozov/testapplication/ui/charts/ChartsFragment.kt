package ru.developer.amukovozov.testapplication.ui.charts

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.RectF
import android.os.Bundle
import android.util.Log
import android.view.View
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.fragment_charts.*
import ru.developer.amukovozov.testapplication.R
import ru.developer.amukovozov.testapplication.data.entity.Store
import ru.developer.amukovozov.testapplication.domain.interactor.StockInteractor
import ru.developer.amukovozov.testapplication.domain.interactor.StoreInteractor
import ru.developer.amukovozov.testapplication.extensions.getAppComponent
import ru.developer.amukovozov.testapplication.presentation.charts.ChartsPresenter
import ru.developer.amukovozov.testapplication.presentation.charts.ChartsView
import ru.developer.amukovozov.testapplication.ui.global.BaseFragment
import ru.terrakok.cicerone.Router
import javax.inject.Inject
import com.github.mikephil.charting.components.MarkerView
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.formatter.IValueFormatter
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.listener.OnChartValueSelectedListener
import com.github.mikephil.charting.utils.MPPointF
import kotlinx.android.synthetic.main.marker_view.view.*


class ChartsFragment : BaseFragment(), ChartsView, OnChartValueSelectedListener {
    override val layoutRes: Int = R.layout.fragment_charts

    @InjectPresenter
    lateinit var presenter: ChartsPresenter

    @Inject
    lateinit var router: Router

    @Inject
    lateinit var storeInteractor: StoreInteractor

    @Inject
    lateinit var stockInteractor: StockInteractor

    @ProvidePresenter
    fun providePresenter() : ChartsPresenter = ChartsPresenter(
        storeInteractor,
        stockInteractor
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        getAppComponent().inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        totalStock.setOnClickListener { presenter.configureTotalStockChart() }
        accuracy.setOnClickListener { presenter.configureAccuracyChart() }
        onFloorAvailability.setOnClickListener { presenter.configureOnFloorAvailabilityChart() }
        meanAge.setOnClickListener { presenter.configureMeanAgeChart() }
    }
    lateinit var storeNames: List<String>

    override fun drawChart(entries: List<BarEntry>, name: String) {
        storeNames = presenter.getStoreNames()
        barChart.setOnChartValueSelectedListener(this)
        val formatter: IValueFormatter = StoreNameValueFormatter(storeNames)
        val barDataSet = BarDataSet(entries, name)
        val barData = BarData(barDataSet)
        barData.setValueFormatter(formatter)
        val mv = XYMarkerView(context)
        mv.chartView = barChart
        barChart.apply {
            marker = mv
            setDrawValueAboveBar(false)
            data = barData
            xAxis.isEnabled = false
            invalidate()
        }
    }

    override fun onValueSelected(e: Entry?, h: Highlight?) {
        val onValueSelectedRectF = RectF()
        if (e == null)
            return

        barChart.getBarBounds(e as BarEntry, onValueSelectedRectF)
        val position = barChart.getPosition(e, YAxis.AxisDependency.LEFT)

        MPPointF.recycleInstance(position)
    }

    override fun onNothingSelected() {}

    override fun showProgress(show: Boolean) {

    }

    override fun showMessage(message: String) {

    }

    @SuppressLint("ViewConstructor")
    inner class XYMarkerView(context: Context?) :
        MarkerView(context, R.layout.marker_view) {

        private val format: StoreNameValueFormatter = StoreNameValueFormatter(storeNames)

        @SuppressLint("SetTextI18n")
        override fun refreshContent(e: Entry?, highlight: Highlight?) {

            tvContent.text = "${format.storeNames[e!!.x.toInt()]}, ${e.y}"

            super.refreshContent(e, highlight)
        }

        override fun getOffset(): MPPointF {
            return MPPointF((-(width / 2)).toFloat(), (-height).toFloat())
        }
    }
}