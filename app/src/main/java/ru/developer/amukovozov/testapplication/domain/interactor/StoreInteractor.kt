package ru.developer.amukovozov.testapplication.domain.interactor

import ru.developer.amukovozov.testapplication.data.entity.Store
import ru.developer.amukovozov.testapplication.domain.repository.StoreRepository
import javax.inject.Inject

class StoreInteractor @Inject constructor(
    private val storeRepository: StoreRepository
) {

    fun getStores(page: Int = 0) = storeRepository.getStores(page)

    fun getStore(storeId: Long) = storeRepository.getStore(storeId)

    fun saveStore(store: Store) = storeRepository.saveStore(store)
}