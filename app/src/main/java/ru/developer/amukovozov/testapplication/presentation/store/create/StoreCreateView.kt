package ru.developer.amukovozov.testapplication.presentation.store.create

import android.support.v7.view.ActionBarPolicy
import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import ru.developer.amukovozov.testapplication.data.entity.Store

interface StoreCreateView : MvpView {

    fun showStore(store: Store)

    fun createStore(store: Store)

    fun showProgress(show: Boolean)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showMessage(message: String)
}