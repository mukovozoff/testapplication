package ru.developer.amukovozov.testapplication.ui.global

import android.view.ViewGroup
import kotlinx.android.synthetic.main.layout_zero.view.*
import ru.developer.amukovozov.testapplication.R
import ru.developer.amukovozov.testapplication.extensions.visible

class ZeroViewHolder(
    private val view: ViewGroup,
    private val refreshListener: () -> Unit = {}
) {
    private val res = view.resources

    init {
        view.refreshButton.setOnClickListener { refreshListener }
    }

    fun showEmptyData() {
        view.titleTextView.text = res.getString(R.string.empty_data)
        view.visible(true)
    }

    fun showEmptyError(msg: String? = null) {
        view.titleTextView.text = res.getText(R.string.empty_error)
        view.visible(true)
    }

    fun hide() {
        view.visible(false)
    }
}