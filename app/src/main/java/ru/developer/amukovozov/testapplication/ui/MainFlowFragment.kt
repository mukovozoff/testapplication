package ru.developer.amukovozov.testapplication.ui

import android.os.Bundle
import com.aurelhubert.ahbottomnavigation.AHBottomNavigation
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationAdapter
import kotlinx.android.synthetic.main.fragment_main.*
import ru.developer.amukovozov.testapplication.R
import ru.developer.amukovozov.testapplication.Screens
import ru.developer.amukovozov.testapplication.extensions.color
import ru.developer.amukovozov.testapplication.ui.global.BaseFragment
import ru.developer.amukovozov.testapplication.ui.global.FlowFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen

class MainFlowFragment : BaseFragment() {

    override val layoutRes: Int = R.layout.fragment_main

    private val currentTabFragment: BaseFragment?
        get() = childFragmentManager.fragments.firstOrNull {!it.isHidden} as BaseFragment?

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        AHBottomNavigationAdapter(activity, R.menu.bottom_menu).apply {
            setupWithBottomNavigation(bottomBar)
        }
        with(bottomBar) {
            accentColor = context.color(R.color.colorPrimary)
            inactiveColor = context.color(R.color.colorPrimaryDark)

            setOnTabSelectedListener { position, wasSelected ->
                if (!wasSelected) selectTab(
                    when (position) {
                        0 -> storesTab
                        else -> chartsTab
                    }
                )
                true
            }
        }

        selectTab(
            when (currentTabFragment?.tag) {
                chartsTab.screenKey -> chartsTab
                else -> storesTab
            }
        )
    }

    private fun selectTab(tab: SupportAppScreen) {
        val currentFragment = currentTabFragment
        val newFragment = childFragmentManager.findFragmentByTag(tab.screenKey)

        if (currentFragment != null && newFragment != null && currentFragment == newFragment) return

        childFragmentManager.beginTransaction().apply {
            if (newFragment == null) add(R.id.mainScreenContainer, createTabFragment(tab), tab.screenKey)

            currentFragment?.let {
                hide(it)
                userVisibleHint = false
            }
            newFragment?.let {
                show(it)
                userVisibleHint = true
            }
        }.commitNow()
    }

    private fun createTabFragment(tab: SupportAppScreen) = tab.fragment

    override fun onBackPressed() {
        currentTabFragment?.onBackPressed()
    }

    companion object {
        private val storesTab = Screens.StoresFlow
        private val chartsTab = Screens.ChartsFlow

    }
}