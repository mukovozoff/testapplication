package ru.developer.amukovozov.testapplication.domain.interactor

import ru.developer.amukovozov.testapplication.data.entity.Stock

class StockInteractor {

    fun calculateTotalStock(stock: Stock) : Int =
        stock.frontStore!! + stock.backStore!! + stock.shoppingWindow!!

}