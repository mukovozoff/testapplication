package ru.developer.amukovozov.testapplication.di.module

import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import ru.developer.amukovozov.testapplication.domain.server.ApiWithCache
import ru.developer.amukovozov.testapplication.domain.server.StoreCache
import ru.developer.amukovozov.testapplication.domain.server.StubApi
import ru.developer.amukovozov.testapplication.domain.system.ServerPath
import ru.developer.amukovozov.testapplication.domain.system.WithErrorHandler
import javax.inject.Singleton

@Module
class ApiModule(@WithErrorHandler private val okHttpClient: OkHttpClient,
                private val gson: Gson,
                private val storeCache: StoreCache,
                @ServerPath private val serverPath: String
) {
    @Provides
    @Singleton
    internal fun provideApi() =
            ApiWithCache(
                Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(okHttpClient)
                    .baseUrl(serverPath)
                    .build()
                    .create(StubApi::class.java),
                storeCache
            )
}