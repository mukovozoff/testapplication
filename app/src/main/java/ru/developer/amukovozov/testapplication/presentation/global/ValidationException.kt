package ru.developer.amukovozov.testapplication.presentation.global

class ValidationException(field: String) : Exception("$field $FIELD_MUST_NOT_BE_EMPTY") {
    companion object {
        private const val FIELD_MUST_NOT_BE_EMPTY = "must not be empty"
    }
}