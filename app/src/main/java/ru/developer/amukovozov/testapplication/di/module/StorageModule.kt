package ru.developer.amukovozov.testapplication.di.module

import dagger.Module
import dagger.Provides
import ru.developer.amukovozov.testapplication.data.AppDatabase
import ru.developer.amukovozov.testapplication.domain.interactor.StockInteractor
import ru.developer.amukovozov.testapplication.domain.interactor.StoreInteractor
import ru.developer.amukovozov.testapplication.domain.repository.StoreRepository
import ru.developer.amukovozov.testapplication.domain.storage.RawAppData
import ru.developer.amukovozov.testapplication.domain.system.SchedulersProvider

@Module
class StorageModule {

    @Provides
    fun provideStoreRepository(rawAppData: RawAppData, schedulers: SchedulersProvider, appDatabase: AppDatabase) =
        StoreRepository(rawAppData, schedulers, appDatabase)

    @Provides
    fun provideStoreInteractor(storeRepository: StoreRepository) =
            StoreInteractor(storeRepository)

    @Provides
    fun provideStockInteractor() =
            StockInteractor()
}