package ru.developer.amukovozov.testapplication.presentation.store.create

import android.content.res.Resources
import android.text.TextUtils
import android.util.Log
import android.widget.Toast
import com.arellomobile.mvp.InjectViewState
import io.reactivex.Observable
import ru.developer.amukovozov.testapplication.data.entity.Store
import ru.developer.amukovozov.testapplication.di.PrimitiveWrapper
import ru.developer.amukovozov.testapplication.domain.interactor.StoreInteractor
import ru.developer.amukovozov.testapplication.presentation.global.BasePresenter
import ru.developer.amukovozov.testapplication.presentation.global.ValidationException
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class StoreCreatePresenter @Inject constructor(
    storeIdWrapper: PrimitiveWrapper<Long>,
    private val storeInteractor: StoreInteractor,
    private val router: Router
) : BasePresenter<StoreCreateView>() {

    private val storedId = storeIdWrapper.value

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        if (storedId != 0L) {
            storeInteractor
                .getStore(storedId)
                .doOnSubscribe { viewState.showProgress(true) }
                .doAfterTerminate { viewState.showProgress(false) }
                .subscribe(
                    {store -> viewState.showStore(store)},
                    {error -> viewState.showMessage(error.localizedMessage)}
                )
                .connect()
        }
    }

    fun createStore(store: Store) {
        storeInteractor
            .saveStore(store)
            .doOnSubscribe { viewState.showProgress(true) }
            .doAfterTerminate { viewState.showProgress(false) }
            .subscribe(
                {
                    databaseSubject.onNext(store)
                    router.exit()
                },
                {error -> viewState.showMessage(error.localizedMessage)}
            )
            .connect()
    }

    fun validateStoreFields(store: Store) : Observable<Store> {
        return Observable.fromCallable {
//            TODO write custom exception
            if (store.name.isEmpty()) {
                throw ValidationException("Store name")
            }
            if (store.countryCode.isEmpty()) {
                throw ValidationException("Country code")
            }
            if (store.email.isEmpty()) {
                throw ValidationException("Email")
            }
            if (store.manager.firstName.isEmpty()) {
                throw ValidationException("Manager firstname")
            }
            if (store.manager.lastName.isEmpty()) {
                throw ValidationException("Manager lastname")
            }
            if (store.manager.managerEmail.isEmpty()) {
                throw ValidationException("Manager email")
            }
            if (store.category == null) {
                throw ValidationException("Category")
            }
            if (store.stock.backStore == null) {
                throw ValidationException("Stock backstore")
            }
            if (store.stock.frontStore == null) {
                throw ValidationException("Stock frontstore")
            }
            if (store.stock.accuracy == null) {
                throw ValidationException("Stock accuracy")
            }
            if (store.stock.shoppingWindow == null) {
                throw ValidationException("Shopping window")
            }
            if (store.onFloorAvailability == null) {
                throw ValidationException("on floor availability")
            }
            if (store.stock.meanAgeDays == null) {
                throw ValidationException("Stock mean age")
            }

            store
        }
    }
}