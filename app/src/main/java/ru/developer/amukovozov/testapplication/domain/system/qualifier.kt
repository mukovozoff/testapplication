package ru.developer.amukovozov.testapplication.domain.system

import javax.inject.Qualifier

@Qualifier
annotation class DefaultPageSize

@Qualifier
annotation class ServerPath

@Qualifier
annotation class CacheLifetime

@Qualifier
annotation class WithErrorHandler
