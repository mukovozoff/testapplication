package ru.developer.amukovozov.testapplication.di

data class PrimitiveWrapper<out T>(val value: T)