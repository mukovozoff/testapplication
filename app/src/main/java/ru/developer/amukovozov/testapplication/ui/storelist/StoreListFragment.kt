package ru.developer.amukovozov.testapplication.ui.storelist

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.Toast
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.hannesdorfmann.adapterdelegates3.ListDelegationAdapter
import kotlinx.android.synthetic.main.fragment_store_create.*
import kotlinx.android.synthetic.main.fragment_stores.*
import kotlinx.android.synthetic.main.layout_base_list.*
import ru.developer.amukovozov.testapplication.R
import ru.developer.amukovozov.testapplication.data.entity.Store
import ru.developer.amukovozov.testapplication.domain.interactor.StoreInteractor
import ru.developer.amukovozov.testapplication.extensions.getAppComponent
import ru.developer.amukovozov.testapplication.extensions.visible
import ru.developer.amukovozov.testapplication.presentation.storelist.StoreListPresenter
import ru.developer.amukovozov.testapplication.presentation.storelist.StoreListView
import ru.developer.amukovozov.testapplication.ui.global.BaseFragment
import ru.developer.amukovozov.testapplication.ui.global.ZeroViewHolder
import ru.developer.amukovozov.testapplication.ui.global.list.ProgressAdapterDelegate
import ru.developer.amukovozov.testapplication.ui.global.list.ProgressItem
import ru.developer.amukovozov.testapplication.ui.global.list.StoresAdapterDelegate
import ru.terrakok.cicerone.Router
import javax.inject.Inject

class StoreListFragment : BaseFragment(), StoreListView {

    private val adapter = StoresAdapter()
    private var zeroViewHolder: ZeroViewHolder? = null

    override val layoutRes: Int = R.layout.fragment_stores

    @Inject
    lateinit var router: Router

    @Inject
    lateinit var interactor: StoreInteractor

    @InjectPresenter
    lateinit var presenter: StoreListPresenter

    @ProvidePresenter
    fun createPresenter(): StoreListPresenter =
            StoreListPresenter(router, interactor)

    override fun onCreate(savedInstanceState: Bundle?) {
        getAppComponent().inject(this)
        super.onCreate(savedInstanceState)
        presenter.onCreate()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.setHasFixedSize(true)
        recyclerView.adapter = adapter

        swipeToRefresh.setOnRefreshListener { presenter.refreshStores() }

        addStore.setOnClickListener { presenter.onCreateStoreClicked() }

    }

    override fun showRefreshProgress(show: Boolean) {
        postViewAction { swipeToRefresh.isRefreshing = show }
    }

    override fun showEmptyProgress(show: Boolean) {
        fullscreenProgressView.visible(show)

        swipeToRefresh.visible(!show)
        postViewAction { swipeToRefresh.isRefreshing = false }
    }

    override fun showPageProgress(show: Boolean) {
        postViewAction { adapter.showProgress(show) }
    }

    override fun showEmptyView(show: Boolean) {
        if (show) {
            zeroViewHolder?.showEmptyData()
        } else {
            zeroViewHolder?.hide()
        }
    }

    override fun showEmptyError(show: Boolean, message: String?) {
        if (show) {
            zeroViewHolder?.showEmptyError(message)
        } else {
            zeroViewHolder?.hide()
        }
    }
    override fun showStores(show: Boolean, stores: List<Store>) {
        recyclerView.visible(show)
        postViewAction { adapter.setData(stores) }
    }

    override fun showErrorMessage(error: Throwable) {
//        TODO realize error handler for ui (toast and snacks)
        Toast.makeText(context, error.localizedMessage, Toast.LENGTH_SHORT).show()
    }

    override fun onBackPressed() = presenter.onBackPressed()

    inner class StoresAdapter : ListDelegationAdapter<MutableList<Any>>() {
        init {
            items = mutableListOf()
            delegatesManager.addDelegate(StoresAdapterDelegate{presenter.onStoreClicked(it.storeId)})
            delegatesManager.addDelegate(ProgressAdapterDelegate())
        }

        fun setData(stores: List<Store>) {
            val progress = isProgress()

            items.clear()
            items.addAll(stores)
            if (progress) items.add(ProgressItem())

            notifyDataSetChanged()
        }

        fun showProgress(isVisible: Boolean) {
            val currentProgress= isProgress()

            if (isVisible && !currentProgress) items.add(ProgressItem())
            else if (!isVisible && currentProgress) items.remove(items.last())
        }

        private fun isProgress() = items.isNotEmpty() && items.last() is ProgressItem

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int, payloads: MutableList<Any?>) {
            super.onBindViewHolder(holder, position, payloads)

            if (position == halfDefaultPageSize()) presenter.loadNextStoresPage()
        }

        private fun halfDefaultPageSize() = items.size - 10
    }
}