package ru.developer.amukovozov.testapplication.domain.server

import io.reactivex.Single
import ru.developer.amukovozov.testapplication.data.entity.Store

interface StubApi {
    fun getStores(page: Int): Single<List<Store>>
}