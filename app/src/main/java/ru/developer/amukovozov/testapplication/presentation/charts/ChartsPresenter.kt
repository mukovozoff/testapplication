package ru.developer.amukovozov.testapplication.presentation.charts

import com.arellomobile.mvp.InjectViewState
import com.github.mikephil.charting.data.BarEntry
import ru.developer.amukovozov.testapplication.data.entity.Store
import ru.developer.amukovozov.testapplication.domain.interactor.StockInteractor
import ru.developer.amukovozov.testapplication.domain.interactor.StoreInteractor
import ru.developer.amukovozov.testapplication.presentation.global.BasePresenter
import javax.inject.Inject

@InjectViewState
class ChartsPresenter @Inject constructor(
    private val storeInteractor: StoreInteractor,
    private val stockInteractor: StockInteractor
) : BasePresenter<ChartsView>() {

    private var stores: List<Store>? = null

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()

        loadStores()
    }

    private fun loadStores() {
        storeInteractor
            .getStores()
            .subscribe(
                {
                    stores = it
                    configureTotalStockChart()
                },
                { error -> viewState.showMessage(error.message!!) }
            )
            .connect()
    }

    fun configureTotalStockChart() {
        val entries = mutableListOf<BarEntry>()

        stores?.let {
            for ((i, store) in stores!!.withIndex()) {
                entries.add(BarEntry(i.toFloat(), stockInteractor.calculateTotalStock(store.stock).toFloat()))
            }
            viewState.drawChart(entries, "Total Stock")
        }

    }

    fun configureAccuracyChart() {
        val entries = mutableListOf<BarEntry>()
        stores?.let {
            for ((i, store) in stores!!.withIndex()) {
                entries.add(BarEntry(i.toFloat(), store.stock.accuracy!!.toFloat()))
            }
            viewState.drawChart(entries, "Accuracy")
        }
    }

    fun configureOnFloorAvailabilityChart() {
        val entries = mutableListOf<BarEntry>()
        stores?.let {
            for ((i, store) in stores!!.withIndex()) {
                entries.add(BarEntry(i.toFloat(), store.onFloorAvailability!!.toFloat()))
            }
            viewState.drawChart(entries, "On Floor Availability")
        }
    }

    fun configureMeanAgeChart() {
        val entries = mutableListOf<BarEntry>()
        stores?.let {
            for ((i, store) in stores!!.withIndex()) {
                entries.add(BarEntry(i.toFloat(), store.stock.meanAgeDays!!.toFloat()))
            }
            viewState.drawChart(entries, "Mean Age")
        }
    }

    fun getStoreNames(): List<String> {
        val storeNames = mutableListOf<String>()
        for (store in stores!!) {
            storeNames.add(store.name)
        }
        return storeNames
    }
}