package ru.developer.amukovozov.testapplication.presentation.global

import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.PublishSubject
import ru.developer.amukovozov.testapplication.data.entity.Store

open class BasePresenter<V : MvpView> : MvpPresenter<V>() {

    private val compositeDisposable = CompositeDisposable()

    companion object {
        val databaseSubject = PublishSubject.create<Store>()
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }

    open fun onCreate() {}

    protected fun Disposable.connect() {
        compositeDisposable.add(this)
    }
}