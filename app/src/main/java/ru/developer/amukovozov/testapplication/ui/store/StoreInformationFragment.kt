package ru.developer.amukovozov.testapplication.ui.store

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.fragment_store_info.*
import ru.developer.amukovozov.testapplication.R
import ru.developer.amukovozov.testapplication.Screens
import ru.developer.amukovozov.testapplication.data.entity.Store
import ru.developer.amukovozov.testapplication.di.PrimitiveWrapper
import ru.developer.amukovozov.testapplication.domain.interactor.StockInteractor
import ru.developer.amukovozov.testapplication.domain.interactor.StoreInteractor
import ru.developer.amukovozov.testapplication.extensions.argument
import ru.developer.amukovozov.testapplication.extensions.getAppComponent
import ru.developer.amukovozov.testapplication.extensions.visible
import ru.developer.amukovozov.testapplication.presentation.store.StoreInformationPresenter
import ru.developer.amukovozov.testapplication.presentation.store.StoreInformationView
import ru.developer.amukovozov.testapplication.ui.global.BaseFragment
import ru.terrakok.cicerone.Router
import timber.log.Timber
import javax.inject.Inject


class StoreInformationFragment : BaseFragment(), StoreInformationView {

    override val layoutRes: Int = R.layout.fragment_store_info

    private val storeId: Long by argument(StoreInformationFragment.ARG_STORE_ID, 0)

    @InjectPresenter
    lateinit var presenter: StoreInformationPresenter

    @Inject
    lateinit var router: Router

    @Inject
    lateinit var storeInteractor: StoreInteractor

    @ProvidePresenter
    fun providePresenter(): StoreInformationPresenter = StoreInformationPresenter(
        PrimitiveWrapper(storeId),
        storeInteractor
    )

    private var disposable: Disposable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        getAppComponent().inject(this)
        super.onCreate(savedInstanceState)
        presenter.onCreate()
    }

    override fun onDestroy() {
        disposable = null
        super.onDestroy()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        toolbar.apply {
            inflateMenu(R.menu.edit_menu)
            setOnMenuItemClickListener { item ->
                when(item.itemId) {
                    R.id.editAction -> router.navigateTo(Screens.StoreCreateFlow(storeId))
                }
                true
            }
        }
    }

    override fun showStore(store: Store) {
        storeName.text = store.name
        countryCode.text = store.countryCode
        storeEmail.text = store.email
        managerFullName.text = store.manager.getFullName()
        managerEmail.text = store.manager.managerEmail
    }

    override fun showProgress(show: Boolean) {
        showProgressDialog(show)
        storeInfoLayout.visible(!show)
    }

    override fun showMessage(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    companion object {
        private const val ARG_STORE_ID = "arg_store_id"

        fun create(storeId: Long) = StoreInformationFragment().apply {
            arguments = Bundle().apply {
                putLong(ARG_STORE_ID, storeId)
            }
        }
    }
}