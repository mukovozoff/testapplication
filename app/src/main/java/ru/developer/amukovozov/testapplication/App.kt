package ru.developer.amukovozov.testapplication

import android.app.Application
import android.arch.persistence.room.Room
import ru.developer.amukovozov.testapplication.data.AppDatabase
import ru.developer.amukovozov.testapplication.di.component.AppComponent
import ru.developer.amukovozov.testapplication.di.component.DaggerAppComponent
import ru.developer.amukovozov.testapplication.di.module.AppModule
import ru.developer.amukovozov.testapplication.di.module.DatabaseModule
import ru.developer.amukovozov.testapplication.di.module.NavigationModule
import ru.developer.amukovozov.testapplication.di.module.StorageModule
import ru.terrakok.cicerone.Cicerone
import timber.log.Timber

class App : Application() {

    val appComponent: AppComponent by lazy {
        val database = Room.databaseBuilder(applicationContext, AppDatabase::class.java, "myDB").build()
        DaggerAppComponent
            .builder()
            .appModule(AppModule(this))
            .databaseModule(DatabaseModule(database))
            .navigationModule(NavigationModule(Cicerone.create()))
            .storageModule(StorageModule())
            .build()
    }

    private fun initLogger() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

    override fun onCreate() {
        super.onCreate()
        initLogger()
    }
}