package ru.developer.amukovozov.testapplication.presentation

import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView
import ru.developer.amukovozov.testapplication.Screens
import ru.terrakok.cicerone.Router
import javax.inject.Inject

class AppPresenter @Inject constructor(
    private val router: Router
) : MvpPresenter<MvpView>() {

    fun start() {
        val rootScreen = Screens.MainFlow
        router.newRootScreen(rootScreen)
    }
}