package ru.developer.amukovozov.testapplication.di.module

import android.content.Context
import android.content.res.AssetManager
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import ru.developer.amukovozov.testapplication.App
import ru.developer.amukovozov.testapplication.di.PrimitiveWrapper
import ru.developer.amukovozov.testapplication.domain.storage.RawAppData
import ru.developer.amukovozov.testapplication.domain.system.AppSchedulers
import ru.developer.amukovozov.testapplication.domain.system.ResourceManager
import ru.developer.amukovozov.testapplication.domain.system.SchedulersProvider
import ru.developer.amukovozov.testapplication.domain.system.ServerPath
import javax.inject.Singleton

@Module
class AppModule(private val app: App) {

    @Singleton
    @Provides
    fun provideApplicationContext() : Context = app.applicationContext

    @Singleton
    @Provides
    fun provideDefaultPageSize() : PrimitiveWrapper<Int> = PrimitiveWrapper(20)

    @Singleton
    @Provides
    fun provideCacheLifeTime() : PrimitiveWrapper<Long> = PrimitiveWrapper(300_000L)

    @Singleton
    @Provides
    fun provideAppSchedulers() : SchedulersProvider = AppSchedulers()

    @Singleton
    @Provides
    @ServerPath
    fun provideServerPath() : String = "serverPath"

    @Singleton
    @Provides
    fun provideAssetManager(context: Context) : AssetManager = context.assets

    @Singleton
    @Provides
    fun provideResourceManager(context: Context) : ResourceManager =
        ResourceManager(context)

    @Singleton
    @Provides
    fun provideRawAppData(assets: AssetManager) : RawAppData =
        RawAppData(assets, Gson())

}