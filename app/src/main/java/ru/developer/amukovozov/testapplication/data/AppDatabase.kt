package ru.developer.amukovozov.testapplication.data

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import ru.developer.amukovozov.testapplication.data.dao.ManagerDao
import ru.developer.amukovozov.testapplication.data.dao.StockDao
import ru.developer.amukovozov.testapplication.data.dao.StoreDao
import ru.developer.amukovozov.testapplication.data.entity.Manager
import ru.developer.amukovozov.testapplication.data.entity.Stock
import ru.developer.amukovozov.testapplication.data.entity.Store

@Database(entities = [Store::class, Stock::class, Manager::class], version = 4)
abstract class AppDatabase : RoomDatabase() {

    abstract fun storeDao(): StoreDao

    abstract fun stockDao(): StockDao

    abstract fun managerDao(): ManagerDao
}