package ru.developer.amukovozov.testapplication.presentation.storelist

import com.arellomobile.mvp.MvpView
import ru.developer.amukovozov.testapplication.data.entity.Store

interface StoreListView : MvpView {

    fun showRefreshProgress(show: Boolean)
    fun showEmptyProgress(show: Boolean)
    fun showPageProgress(show: Boolean)
    fun showEmptyView(show: Boolean)
    fun showStores(show: Boolean, stores: List<Store>)
    fun showErrorMessage(error: Throwable)
    fun showEmptyError(show: Boolean, message: String?)
}