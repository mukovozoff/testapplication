package ru.developer.amukovozov.testapplication.data.dao

import android.arch.persistence.room.*
import ru.developer.amukovozov.testapplication.data.entity.Manager

@Dao
interface ManagerDao {

    @Query("SELECT * from Manager")
    fun getAll(): List<Manager>

    @Query("SELECT * from Manager where managerId == :id")
    fun getById(id: Long) : Manager

    @Query("SELECT * from Manager where managerEmail == :email")
    fun getByEmail(email: String) : Manager

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(manager: Manager) : Long

    @Delete
    fun delete(manager: Manager)

    @Query("DELETE from Manager")
    fun deleteAll()
}