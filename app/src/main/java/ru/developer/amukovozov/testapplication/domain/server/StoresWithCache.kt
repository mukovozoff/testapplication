package ru.developer.amukovozov.testapplication.domain.server

import io.reactivex.Single
import ru.developer.amukovozov.testapplication.data.AppDatabase
import ru.developer.amukovozov.testapplication.data.entity.Store
import ru.developer.amukovozov.testapplication.domain.storage.RawAppData

class StoresWithCache(
    private val rawAppData: RawAppData,
    private val storeCache: StoreCache,
    private val database: AppDatabase
) {

}
//) : StubApi by rawAppData {
//
//    override fun getStores(page: Int): Single<List<Store>> =
//        rawAppData.getStores(page)
//            .doOnSuccess { storesList ->
//                storeCache.put(storesList)
//                val storeDao = database.storeDao()
//                storesList.forEach { storeDao.insert(it) }
//            }
//
//}