package ru.developer.amukovozov.testapplication.presentation.charts

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.github.mikephil.charting.data.BarEntry
import ru.developer.amukovozov.testapplication.data.entity.Store

interface ChartsView : MvpView{

    fun drawChart(entries: List<BarEntry>, name: String)
    fun showProgress(show: Boolean)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showMessage(message: String)
}