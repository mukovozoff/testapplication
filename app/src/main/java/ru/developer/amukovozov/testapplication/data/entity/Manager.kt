package ru.developer.amukovozov.testapplication.data.entity

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
class Manager(@PrimaryKey(autoGenerate = true) var managerId: Long = 0,
                   @SerializedName("First Name") val firstName: String,
                   @SerializedName("Last Name") val lastName: String,
                   @SerializedName("Email") val managerEmail: String) {

    fun getFullName() : String {
        return "$firstName $lastName"
    }
}