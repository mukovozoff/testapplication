package ru.developer.amukovozov.testapplication.presentation.store

import com.arellomobile.mvp.MvpView
import ru.developer.amukovozov.testapplication.presentation.global.BasePresenter
import ru.terrakok.cicerone.Router
import javax.inject.Inject

class StoreFlowPresenter @Inject constructor(
    private val router: Router
) : BasePresenter<MvpView>() {

    fun onExit() {
        router.exit()
    }
}