package ru.developer.amukovozov.testapplication.data.dao

import android.arch.persistence.room.*
import ru.developer.amukovozov.testapplication.data.entity.Manager
import ru.developer.amukovozov.testapplication.data.entity.Stock

@Dao
interface StockDao {
    @Query("SELECT * from Stock")
    fun getAll(): List<Stock>

    @Query("SELECT * from Stock where stockId == :id")
    fun getById(id: Long) : Stock

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(stock: Stock) : Long

    @Delete
    fun delete(stock: Stock)

    @Query("DELETE from Stock")
    fun deleteAll()
}